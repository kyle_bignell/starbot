﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using POSH_Bot.util;
using POSH_Core.sys;
using POSH_Core.sys.annotations;

namespace POSH_Bot
{
    /// <summary>
    /// The status behaviour has primitives for the state of the game 
    /// </summary>
    public class Status : BotBehaviour
    {
		public Status(AgentBase agent) : base(agent, 
                                              new string[] {"Action_Idle"},
		                                      new string[] {"Sense_Fail",
                                                            "Sense_Succeed",
                                                            "Sense_Game_Ended",
                                                            "Sense_Mineral_Count",
                                                            "Sense_Gas_Count",
                                                            "Sense_Geyser_Count",
                                                            "Sense_Worker_Count",
                                                            "Sense_Marine_Count",
                                                            "Sense_Medic_Count",
                                                            "Sense_Barracks_Count",
                                                            "Sense_Need_Supply",
                                                            "Sense_Building_In_Progress",
                                                            "Sense_Building_Supply",
                                                            "Sense_Building_Barracks",
                                                            "Sense_Have_Barracks",
                                                            "Sense_Building_Academy",
                                                            "Sense_Have_Academy"})
        {}

		// ACTIONS

        [ExecutableAction("Action_Idle")]
        public bool Action_Idle()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Action_Idle");
            }

            GetBot().issueCommand("Action_Idle");

            return true;
        }

        // SENSES

        [ExecutableSense("Sense_Fail")]
        public bool Sense_Fail()
        {
            return false;
        }

        [ExecutableSense("Sense_Succeed")]
        public bool Sense_Succeed()
        {
            return true;
        }

        [ExecutableSense("Sense_Game_Ended")]
        public bool Sense_Game_Ended()
		{
            if (_debug_)
            {
                Console.Out.WriteLine("in Sense_Game_Ended");
            }

            if (GetBot().killConnection)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Mineral_Count")]
        public int Sense_Mineral_Count()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Mineral_Count");
            }

            GetBot().issueCommand("Sense_Mineral_Count");

            int minerals = Int32.Parse(GetBot().getReply());
            GetBot().replyHandled();

            return minerals;
        }

        [ExecutableSense("Sense_Gas_Count")]
        public int Sense_Gas_Count()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Gas_Count");
            }

            GetBot().issueCommand("Sense_Gas_Count");

            int gas = Int32.Parse(GetBot().getReply());
            GetBot().replyHandled();

            return gas;
        }

        [ExecutableSense("Sense_Geyser_Count")]
        public int Sense_Geyser_Count()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Geyser_Count");
            }

            GetBot().issueCommand("Sense_Geyser_Count");

            int geyser_count = Int32.Parse(GetBot().getReply());
            GetBot().replyHandled();

            return geyser_count;
        }

        [ExecutableSense("Sense_Worker_Count")]
        public int Sense_Worker_Count()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Worker_Count");
            }

            GetBot().issueCommand("Sense_Worker_Count");

            int worker_count = Int32.Parse(GetBot().getReply());
            GetBot().replyHandled();

            return worker_count;
        }

        [ExecutableSense("Sense_Marine_Count")]
        public int Sense_Marine_Count()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Marine_Count");
            }

            GetBot().issueCommand("Sense_Marine_Count");

            int marine_count = Int32.Parse(GetBot().getReply());
            GetBot().replyHandled();

            return marine_count;
        }

        [ExecutableSense("Sense_Medic_Count")]
        public int Sense_Medic_Count()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Medic_Count");
            }

            GetBot().issueCommand("Sense_Medic_Count");

            int medic_count = Int32.Parse(GetBot().getReply());
            GetBot().replyHandled();

            return medic_count;
        }

        [ExecutableSense("Sense_Barracks_Count")]
        public int Sense_Barracks_Count()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Barracks_Count");
            }

            GetBot().issueCommand("Sense_Barracks_Count");

            int barracks_count = Int32.Parse(GetBot().getReply());
            GetBot().replyHandled();

            return barracks_count;
        }

        [ExecutableSense("Sense_Need_Supply")]
        public bool Sense_Need_Supply()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Need_Supply");
            }

            GetBot().issueCommand("Sense_Need_Supply");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Building_In_Progress")]
        public bool Sense_Building_In_Progress()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Building_In_Progress");
            }

            GetBot().issueCommand("Sense_Building_In_Progress");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Building_Supply")]
        public bool Sense_Building_Supply()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Building_Supply");
            }

            GetBot().issueCommand("Sense_Building_Supply");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Building_Barracks")]
        public bool Sense_Building_Barracks()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Building_Barracks");
            }

            GetBot().issueCommand("Sense_Building_Barracks");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Have_Barracks")]
        public bool Sense_Have_Barracks()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Have_Barracks");
            }

            GetBot().issueCommand("Sense_Have_Barracks");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Building_Academy")]
        public bool Sense_Building_Academy()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Building_Academy");
            }

            GetBot().issueCommand("Sense_Building_Academy");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Have_Academy")]
        public bool Sense_Have_Academy()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Have_Academy");
            }

            GetBot().issueCommand("Sense_Have_Academy");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}