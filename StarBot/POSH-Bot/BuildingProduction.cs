﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using POSH_Bot.util;
using POSH_Core.sys;
using POSH_Core.sys.strict;
using POSH_Core.sys.annotations;

namespace POSH_Bot
{
    public class BuildingProduction : BotBehaviour
    {
        public BuildingProduction(AgentBase agent) : base(agent, 
                                                          new string[] {"Action_Build_Supply",
                                                                        "Action_Build_Refinery",
                                                                        "Action_Build_Barracks",
                                                                        "Action_Build_Academy"},
                                                          new string[] {"Sense_Can_Produce_Supply",
                                                                        "Sense_Can_Produce_Refinery",
                                                                        "Sense_Can_Produce_Barracks",
                                                                        "Sense_Can_Produce_Academy"})
        {}

        // ACTIONS

        [ExecutableAction("Action_Build_Supply")]
        public bool Action_Build_Supply()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Action_Build_Supply");
            }

            GetBot().issueCommand("Action_Build_Supply");

            return true;
        }

        [ExecutableAction("Action_Build_Refinery")]
        public bool Action_Build_Refinery()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Action_Build_Refinery");
            }

            GetBot().issueCommand("Action_Build_Refinery");

            return true;
        }

        [ExecutableAction("Action_Build_Barracks")]
        public bool Action_Build_Barracks()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Action_Build_Barracks");
            }

            GetBot().issueCommand("Action_Build_Barracks");

            return true;
        }

        [ExecutableAction("Action_Build_Academy")]
        public bool Action_Build_Academy()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Action_Build_Academy");
            }

            GetBot().issueCommand("Action_Build_Academy");

            return true;
        }

        // SENSES

        [ExecutableSense("Sense_Can_Produce_Supply")]
        public bool Sense_Can_Produce_Supply()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Can_Produce_Supply");
            }

            GetBot().issueCommand("Sense_Can_Produce_Supply");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Can_Produce_Refinery")]
        public bool Sense_Can_Produce_Refinery()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Can_Produce_Refinery");
            }

            GetBot().issueCommand("Sense_Can_Produce_Refinery");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Can_Produce_Barracks")]
        public bool Sense_Can_Produce_Barracks()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Can_Produce_Barracks");
            }

            GetBot().issueCommand("Sense_Can_Produce_Barracks");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Can_Produce_Academy")]
        public bool Sense_Can_Produce_Academy()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Can_Produce_Academy");
            }

            GetBot().issueCommand("Sense_Can_Produce_Academy");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}