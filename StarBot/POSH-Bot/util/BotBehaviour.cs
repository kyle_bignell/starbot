﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using POSH_Core.sys;

namespace POSH_Bot.util
{
    public class BotBehaviour : Behaviour
    {
        public BotBehaviour(AgentBase agent, string[] actions, string[] senses) : base(agent, actions, senses)
        {

        }

        protected POSHBot GetBot()
        {
			return ((POSHBot)agent.getBehaviour("POSHBot"));
        }

        protected Status GetStatus()
        {
            return ((Status)agent.getBehaviour("Status"));
        }

        protected Status GetBuildingProduction()
        {
            return ((Status)agent.getBehaviour("BuildingProduction"));
        }

        protected Status GetUnitControl()
        {
            return ((Status)agent.getBehaviour("UnitControl"));
        }

        protected Status GetUnitProduction()
        {
            return ((Status)agent.getBehaviour("UnitProduction"));
        }
    }
}
