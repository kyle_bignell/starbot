﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using POSH_Bot.util;
using POSH_Core.sys;
using POSH_Core.sys.strict;
using POSH_Core.sys.annotations;

namespace POSH_Bot
{
    public class UnitControl : BotBehaviour
    {
        public UnitControl(AgentBase agent) : base(agent, 
                                                   new string[] {"Action_Manage_Workers",
                                                                 "Action_Send_Idle_Workers_To_Mine"},
                                                   new string[] {"Sense_Idle_Workers"})
        {}

        // ACTIONS

        [ExecutableAction("Action_Manage_Workers")]
        public bool Action_Manage_Workers()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Action_Manage_Workers");
            }

            GetBot().issueCommand("Action_Manage_Workers");

            return true;
        }

        [ExecutableAction("Action_Send_Idle_Workers_To_Mine")]
        public bool Action_Send_Idle_Workers_To_Mine()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Action_Send_Idle_Workers_To_Mine");
            }

            GetBot().issueCommand("Action_Send_Idle_Workers_To_Mine");

            return true;
        }

        // SENSES

        [ExecutableSense("Sense_Idle_Workers")]
        public bool Sense_Idle_Workers()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Idle_Workers");
            }

            GetBot().issueCommand("Sense_Idle_Workers");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}