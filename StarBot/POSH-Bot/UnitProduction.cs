﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using POSH_Bot.util;
using POSH_Core.sys;
using POSH_Core.sys.strict;
using POSH_Core.sys.annotations;

namespace POSH_Bot
{
    public class UnitProduction : BotBehaviour
    {
        public UnitProduction(AgentBase agent) : base(agent, 
                                                      new string[] {"Action_Produce_SCV",
                                                                    "Action_Produce_Marine",
                                                                    "Action_Produce_Medic"},
                                                      new string[] {"Sense_Can_Produce_SCV",
                                                                    "Sense_Can_Produce_Marine",
                                                                    "Sense_Can_Produce_Medic"})
        {}

        // ACTIONS

        [ExecutableAction("Action_Produce_SCV")]
        public bool Action_Produce_SCV()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Action_Produce_SCV");
            }

            GetBot().issueCommand("Action_Produce_SCV");

            return true;
        }

        [ExecutableAction("Action_Produce_Marine")]
        public bool Action_Produce_Marine()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Action_Produce_Marine");
            }

            GetBot().issueCommand("Action_Produce_Marine");

            return true;
        }

        [ExecutableAction("Action_Produce_Medic")]
        public bool Action_Produce_Medic()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Action_Produce_Medic");
            }

            GetBot().issueCommand("Action_Produce_Medic");

            return true;
        }

        // SENSES

        [ExecutableSense("Sense_Can_Produce_SCV")]
        public bool Sense_Can_Produce_SCV()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Can_Produce_SCV");
            }

            GetBot().issueCommand("Sense_Can_Produce_SCV");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Can_Produce_Marine")]
        public bool Sense_Can_Produce_Marine()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Can_Produce_Marine");
            }

            GetBot().issueCommand("Sense_Can_Produce_Marine");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [ExecutableSense("Sense_Can_Produce_Medic")]
        public bool Sense_Can_Produce_Medic()
        {
            if (_debug_)
            {
                Console.Out.WriteLine(" in Sense_Can_Produce_Medic");
            }

            GetBot().issueCommand("Sense_Can_Produce_Medic");

            string reply = GetBot().getReply();
            GetBot().replyHandled();

            if (reply.Contains("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}     

    
