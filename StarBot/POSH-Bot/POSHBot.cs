﻿using System;
using System.IO;
using System.Net;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using POSH_Bot.util;
using POSH_Core.sys;
using POSH_Core.sys.strict;

namespace POSH_Bot
{
    //POSHBot created as a means of evaluating Behaviour Oriented Design [BOD]
    //Much code here re-used from Andy Kwong's poshbot
    //It has been refactored on the 29/08/07 to make Bot a behaviour and clean
    //up the behaviour structure a bit.
     
    /// <summary>
    /// The Bot behaviour.
    ///    
    /// This behaviour does not provide any actions that are directly used in plans.
    /// Rather, it establishes the connection with UT and provides methods to
    /// control the bot which can be used by other behaviours.
    /// 
    /// The behaviour keeps a local copy of the bot state. Gamebots do not support
    /// queries on the agent sense, it sends a copy of the environment to the
    /// agent periodically.
    /// 
    /// To change connection IP, port and the bot's name, use the attributes
    /// Bot.ip, Bot.port and Bot.botname.
    /// </summary>
    public class POSHBot : BotBehaviour
    {
        int port;
        IPAddress ip;
        Thread connectionThread;

        string command;
        string reply;

        protected internal Semaphore commandReady { get; set; }
        protected internal Semaphore replyReady { get; set; }
        protected internal Semaphore replyProcessed { get; set; }
		protected internal bool killConnection { get; private set;}

        public POSHBot(AgentBase agent) : base(agent, new string[] { }, new string[] { })
        {
            // Default connection values, use attributes to override
            port = 3000;
            ip = IPAddress.Parse("127.0.0.1");
            connectionThread = null;

            command = string.Empty;
            reply = string.Empty;

            // Signals if command is free to be used
            commandReady = new Semaphore(1, 1);
            // Signals if reply is ready to be processed by a behaviour module
            replyReady = new Semaphore(1, 1);
            // Signals if the reply has been processed by a behaviour module
            replyProcessed = new Semaphore(1, 1);
            // Indicates whether connection thread can be killed
            killConnection = false;
        }

        /// <summary>
        /// Attempts connecting to the BWAPI server.
        /// 
        /// If the bot is currently connected, it disconnects, waits a bit,
        /// and then reconnects.
        /// </summary>
        public override bool Reset()
        {
            // Disconnect
            if (connectionThread != null)
            {
                log.Debug("Currently connected, trying to disconnect");
                
                Disconnect();

                // Wait for 3 seconds to disconnection
                int timeout = 0;
                while ((connectionThread != null) && timeout++ < 30)
                {
                    Thread.Sleep(10);
                }
            }

            // Connect only if not connected
            if (connectionThread == null)
            {
                Connect();
                return true;
            }
            else
            {
                log.Error("Reset failed - failed to disconnect");
                return false;
            }
        }

        /// <summary>
        /// Returns if the behaviour is ready.
        /// 
        /// This method is called by agent.loopThread to make sure that the
        /// behaviour is OK in every cycle. This behaviour is OK if it is connected
        /// to BWAPI.
        /// </summary>
        public override bool CheckError()
        {
            if (killConnection == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Prepares the bot to exit by disconnecting from UT.
        /// </summary>
        public override void  ExitPrepare()
        {
 	         Disconnect();
        }

        /// <summary>
        /// Calls connect_thread in a new thread
        /// </summary>
        public bool Connect()
        {
            log.Info(string.Format("Connecting to Server ({0}:{1})", ip, port));

            if(connectionThread == null)
            {
                connectionThread = new Thread(this.ConnectThread);
                connectionThread.Start();
                return true;
            }
            else
            {
                log.Error("Attempting to Connect() when thread already active");
                return false;
            }
        }

        public void Disconnect()
        {
            killConnection = true;
        }

        public void issueCommand(string newCommand)
        {
            // Block access to command and reply until we are finished with them
            this.commandReady.WaitOne();
            this.replyReady.WaitOne();

            // Set the new command
            this.command = newCommand;
        }

        public string getReply()
        {
            // Wait for reply to be ready
            this.replyReady.WaitOne();
            this.replyReady.Release();

            // Signal that the reply is being processed by behaviour
            this.replyProcessed.WaitOne();

            return this.reply;
        }

        public void signalReplyProcessed()
        {
            this.replyProcessed.Release();
        }

        private void resetCommand()
        {
            // Signal the command is free and reset it
            this.commandReady.Release();
            this.command = string.Empty;
        }

        public void replyHandled()
        {
            // Signal the reply has been handled and reset it
            this.replyProcessed.Release();
            this.reply = string.Empty;
        }

        private string ReadMessage(NetworkStream stream)
        {
            Byte[] data = new Byte[256];
            String message = String.Empty;
            
            try
            {
                Int32 bytes = stream.Read(data, 0, data.Length);
                message = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
            }
            catch (Exception)
            {
                log.Error("Error reading message from server");
                killConnection = true;
            }

            Console.WriteLine("Read message: " + message);

            return message;
        }

        private void WriteMessage(NetworkStream stream, String message)
        {
            Console.WriteLine("Writing message: " + message);

            Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);

            try
            {
                stream.Write(data, 0, data.Length);
            }
            catch (Exception)
            {
                log.Error("Error writing message to server");
                killConnection = true;
            }
        }

        /// <summary>
        /// This method runs inside a thread updating the agent state
        /// by reading from the network socket
        /// </summary>
        void ConnectThread()
        {
            TcpClient client = null;
            NetworkStream stream = null;

            // Connect to BWAPI server
            try
            {
                client = new TcpClient(ip.ToString(), port);

                if(client.Connected)
                {
                    stream = client.GetStream();
                    log.Info("Connection successful");
                }
                else
                {
                    log.Error("Connection to server failed");
                    killConnection = true;
                    return;
                }
            }
            catch (Exception)
            {
                log.Error("Connection to server failed");
                this.killConnection = true;
                return;
            }

            this.reply = this.ReadMessage(stream);

            // Main Loop
            while (!this.killConnection)
            {
                if (this.command != string.Empty)
                {
                    WriteMessage(stream, this.command);

                    this.reply = ReadMessage(stream);

                    // Reply is ready to be accessed
                    this.replyReady.Release();
                    
                    // Connection closed
                    if (this.reply == string.Empty)
                    {
                        log.Error("Connection closed by server");
                        killConnection = true;
                        break;
                    }
                    // Handle response
                    else
                    {
                        // Wait for reply to be processed by behaviour
                        this.replyProcessed.WaitOne();
                        this.replyProcessed.Release();
                    }

                    resetCommand();
                }
                else
                {
                    // Send idle message
                    WriteMessage(stream, "Action_Idle");

                    // Read response
                    this.reply = ReadMessage(stream);
                    
                    // Connection closed
                    if (this.reply == string.Empty)
                    {
                        log.Error("Connection closed by server");
                        killConnection = true;
                        break;
                    }
                }
            }

            log.Info("Closing connection and cleaning up...");

            try
            {
                if (client is TcpClient && client.Connected)
                {
                    client.GetStream().Close();
                    client.Close();
                }
            }
            catch (Exception)
            {
                log.Error("Closing connection to server failed.");
                this.killConnection = true;
                return;
            }

            log.Info("Connection thread terminating.");
            return;
        }
    }
}