#include "BWAPIBot.h"


BWAPIBot::BWAPIBot()
{
	tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), 3000));

	// Wait for POSHBot to connect
	try
	{
		std::cout << "Waiting for POSHBot to connect..." << std::endl;

		acceptor.accept(socket);

		std::cout << "POSHBot connected" << std::endl;
	}
	catch (std::exception &ex)
	{
		std::cout << "Error waiting for POSHBot to connect" << std::endl;
		std::cerr << ex.what() << std::endl;
	}

	std::cout << "Connecting to Starcraft..." << std::endl;

	reconnect();

	show_bullets = false;
	show_visibility_data = false;
	building_begin = 0;
	building_wait_time = 10;
	building_in_progress = false;
	message.fill(' ');
}

BWAPIBot::~BWAPIBot()
{
}

void BWAPIBot::reconnect()
{
	std::cout << "Reconnecting..." << std::endl;
	
	while (!BWAPIClient.connect())
	{
		std::this_thread::sleep_for(std::chrono::milliseconds{ 1000 });
	}
}

void BWAPIBot::enter_match()
{
	std::cout << "Waiting to enter match" << std::endl;

	while (!BWAPI::Broodwar->isInGame())
	{
		BWAPI::BWAPIClient.update();

		if (!BWAPI::BWAPIClient.isConnected())
		{
			std::cout << "Reconnecting..." << std::endl;;
			reconnect();
		}
	}

	std::cout << "Match starting" << std::endl;

	BWAPI::Broodwar << "The map is " << BWAPI::Broodwar->mapName() << ", a " << BWAPI::Broodwar->getStartLocations().size() << " player map" << std::endl;

	if (BWAPI::Broodwar->enemy())
	{
		BWAPI::Broodwar << "The match up is " << BWAPI::Broodwar->self()->getRace() << " vs " << BWAPI::Broodwar->enemy()->getRace() << std::endl;
	}
}

bool BWAPIBot::send_message(string message)
{
	std::cout << "Sending message: " << message << std::endl;

	try
	{
		boost::asio::write(socket, boost::asio::buffer(message), error);
		std::cout << "Sent message" << std::endl;
		return true;
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return false;
	}
}

bool BWAPIBot::read_message()
{
	size_t message_length = socket.read_some(boost::asio::buffer(message), error);

	// Connection closed cleanly by peer.
	if (error == boost::asio::error::eof)
	{
		return false;
	}
	// Some other error.
	else if (error)
	{
		std::cout << "Error reading message" << std::endl;
		message.fill(' ');
	}

	std::cout << "Read message: " << message.c_array() << std::endl;

	return true;
}

char* BWAPIBot::get_message()
{
	return message.c_array();
}

string BWAPIBot::handle_message()
{
	string reply;

	if (strstr(get_message(), "Action") != NULL)
	{
		reply = handle_action();
	}
	else if (strstr(get_message(), "Sense") != NULL)
	{
		reply = handle_sense();
	}
	else
	{
		reply = "Unknown";
	}

	return reply;
}

void BWAPIBot::reset_message()
{
	message.fill(' ');
}

void BWAPIBot::handle_game_events()
{
	for (auto &e : BWAPI::Broodwar->getEvents())
	{
		switch (e.getType())
		{
			case EventType::MatchEnd:
				if (e.isWinner())
					BWAPI::Broodwar << "I won the game" << std::endl;
				else
					BWAPI::Broodwar << "I lost the game" << std::endl;
				break;

			case EventType::SendText:
				if (e.getText() == "/show bullets")
				{
					show_bullets = !show_bullets;
				}
				else if (e.getText() == "/show players")
				{
					showPlayers();
				}
				else if (e.getText() == "/show forces")
				{
					showForces();
				}
				else if (e.getText() == "/show visibility")
				{
					show_visibility_data = !show_visibility_data;
				}
				else
				{
					BWAPI::Broodwar << "You typed \"" << e.getText() << "\"!" << std::endl;
				}
				break;

			case EventType::ReceiveText:
				BWAPI::Broodwar << e.getPlayer()->getName() << " said \"" << e.getText() << "\"" << std::endl;
				break;

			case EventType::PlayerLeft:
				BWAPI::Broodwar << e.getPlayer()->getName() << " left the game." << std::endl;
				break;

			case EventType::NukeDetect:
				if (e.getPosition() != Positions::Unknown)
				{
					BWAPI::Broodwar->drawCircleMap(e.getPosition(), 40, Colors::Red, true);
					BWAPI::Broodwar << "Nuclear Launch Detected at " << e.getPosition() << std::endl;
				}
				else
				{
					BWAPI::Broodwar << "Nuclear Launch Detected" << std::endl;
				}
				break;

			case EventType::UnitCreate:
				if (!BWAPI::Broodwar->isReplay())
				{
					BWAPI::Broodwar << "A " << e.getUnit()->getType() << " [" << e.getUnit() << "] has been created at " << e.getUnit()->getPosition() << std::endl;
				}
				else
				{
					// if we are in a replay, then we will print out the build order
					// (just of the buildings, not the units).
					if (e.getUnit()->getType().isBuilding() && e.getUnit()->getPlayer()->isNeutral() == false)
					{
						int seconds = BWAPI::Broodwar->getFrameCount() / 24;
						int minutes = seconds / 60;
						seconds %= 60;
						BWAPI::Broodwar->sendText("%.2d:%.2d: %s creates a %s", minutes, seconds, e.getUnit()->getPlayer()->getName().c_str(), e.getUnit()->getType().c_str());
					}
				}
				break;

			case EventType::UnitDestroy:
				if (!BWAPI::Broodwar->isReplay())
				{
					BWAPI::Broodwar->sendText("A %s [%p] has been destroyed at (%d,%d)", e.getUnit()->getType().c_str(), e.getUnit(), e.getUnit()->getPosition().x, e.getUnit()->getPosition().y);
				}
				break;

			case EventType::UnitMorph:
				if (!BWAPI::Broodwar->isReplay())
				{
					BWAPI::Broodwar->sendText("A %s [%p] has been morphed at (%d,%d)", e.getUnit()->getType().c_str(), e.getUnit(), e.getUnit()->getPosition().x, e.getUnit()->getPosition().y);
				}
				else
				{
					// if we are in a replay, then we will print out the build order
					// (just of the buildings, not the units).
					if (e.getUnit()->getType().isBuilding() && e.getUnit()->getPlayer()->isNeutral() == false)
					{
						int seconds = BWAPI::Broodwar->getFrameCount() / 24;
						int minutes = seconds / 60;
						seconds %= 60;
						BWAPI::Broodwar->sendText("%.2d:%.2d: %s morphs a %s", minutes, seconds, e.getUnit()->getPlayer()->getName().c_str(), e.getUnit()->getType().c_str());
					}
				}
				break;

			case EventType::UnitShow:
				if (!BWAPI::Broodwar->isReplay())
				{
					BWAPI::Broodwar->sendText("A %s [%p] has been spotted at (%d,%d)", e.getUnit()->getType().c_str(), e.getUnit(), e.getUnit()->getPosition().x, e.getUnit()->getPosition().y);
				}
				break;

			case EventType::UnitHide:
				if (!BWAPI::Broodwar->isReplay())
				{
					BWAPI::Broodwar->sendText("A %s [%p] was last seen at (%d,%d)", e.getUnit()->getType().c_str(), e.getUnit(), e.getUnit()->getPosition().x, e.getUnit()->getPosition().y);
				}
				break;

			case EventType::UnitRenegade:
				if (!BWAPI::Broodwar->isReplay())
				{
					BWAPI::Broodwar->sendText("A %s [%p] is now owned by %s", e.getUnit()->getType().c_str(), e.getUnit(), e.getUnit()->getPlayer()->getName().c_str());
				}
				break;

			case EventType::SaveGame:
				BWAPI::Broodwar->sendText("The game was saved to \"%s\".", e.getText().c_str());
				break;
		}
	}

	if (show_bullets)
	{
		drawBullets();
	}

	if (show_visibility_data)
	{
		drawVisibilityData();
	}

	drawStats();

	BWAPI::Broodwar->drawTextScreen(300, 0, "FPS: %f", BWAPI::Broodwar->getAverageFPS());
}

void BWAPIBot::update()
{
	if (building_in_progress)
	{
		// Reset building in progress flag if enough time has passed
		if ((BWAPI::Broodwar->elapsedTime() - building_begin) >= building_wait_time)
		{
			building_begin = 0;
			building_in_progress = false;
		}
	}
}

void BWAPIBot::drawStats()
{
	int line = 1;
	BWAPI::Broodwar->drawTextScreen(5, 0, "I have %d units:", BWAPI::Broodwar->self()->allUnitCount());

	for (auto& unitType : UnitTypes::allUnitTypes())
	{
		int count = BWAPI::Broodwar->self()->allUnitCount(unitType);
		if (count)
		{
			BWAPI::Broodwar->drawTextScreen(5, 16 * line, "- %d %s%c", count, unitType.c_str(), count == 1 ? ' ' : 's');
			++line;
		}
	}
}

void BWAPIBot::drawBullets()
{
	for (auto &b : BWAPI::Broodwar->getBullets())
	{
		Position p = b->getPosition();
		double velocityX = b->getVelocityX();
		double velocityY = b->getVelocityY();
		BWAPI::Broodwar->drawLineMap(p, p + Position((int)velocityX, (int)velocityY), b->getPlayer() == BWAPI::Broodwar->self() ? Colors::Green : Colors::Red);
		BWAPI::Broodwar->drawTextMap(p, "%c%s", b->getPlayer() == BWAPI::Broodwar->self() ? Text::Green : Text::Red, b->getType().c_str());
	}
}

void BWAPIBot::drawVisibilityData()
{
	int wid = BWAPI::Broodwar->mapHeight(), hgt = BWAPI::Broodwar->mapWidth();
	for (int x = 0; x < wid; ++x)
	{
		for (int y = 0; y < hgt; ++y)
		{
			if (BWAPI::Broodwar->isExplored(x, y))
				BWAPI::Broodwar->drawDotMap(x * 32 + 16, y * 32 + 16, BWAPI::Broodwar->isVisible(x, y) ? Colors::Green : Colors::Blue);
			else
				BWAPI::Broodwar->drawDotMap(x * 32 + 16, y * 32 + 16, Colors::Red);
		}
	}
}

void BWAPIBot::showPlayers()
{
	Playerset players = BWAPI::Broodwar->getPlayers();
	for (auto p : players)
	{
		BWAPI::Broodwar << "Player [" << p->getID() << "]: " << p->getName() << " is in force: " << p->getForce()->getName() << std::endl;
	}
}

void BWAPIBot::showForces()
{
	Forceset forces = BWAPI::Broodwar->getForces();
	for (auto f : forces)
	{
		Playerset players = f->getPlayers();
		BWAPI::Broodwar << "Force " << f->getName() << " has the following players:" << std::endl;
		for (auto p : players)
		{
			BWAPI::Broodwar << "  - Player [" << p->getID() << "]: " << p->getName() << std::endl;
		}
	}
}

string BWAPIBot::handle_action()
{
	string reply;

	if (strstr(get_message(), "Action_Manage_Workers") != NULL)
	{
		Action_Manage_Workers();
		reply = "Succeed";
	}
	else if(strstr(get_message(), "Action_Send_Idle_Workers_To_Mine") != NULL)
	{
		Action_Send_Idle_Workers_To_Mine();
		reply = "Succeed";
	}
	else if (strstr(get_message(), "Action_Produce_SCV") != NULL)
	{
		Action_Produce_SCV();
		reply = "Succeed";
	}
	else if (strstr(get_message(), "Action_Produce_Marine") != NULL)
	{
		Action_Produce_Marine();
		reply = "Succeed";
	}
	else if (strstr(get_message(), "Action_Produce_Medic") != NULL)
	{
		Action_Produce_Medic();
		reply = "Succeed";
	}
	else if (strstr(get_message(), "Action_Build_Supply") != NULL)
	{
		Action_Build_Supply();
		reply = "Succeed";
	}
	else if (strstr(get_message(), "Action_Build_Refinery") != NULL)
	{
		Action_Build_Refinery();
		reply = "Succeed";
	}
	else if (strstr(get_message(), "Action_Build_Barracks") != NULL)
	{
		Action_Build_Barracks();
		reply = "Succeed";
	}
	else if (strstr(get_message(), "Action_Build_Academy") != NULL)
	{
		Action_Build_Academy();
		reply = "Succeed";
	}
	else if (strstr(get_message(), "Action_Idle") != NULL)
	{
		reply = "Idle";
	}

	return reply;
}

string BWAPIBot::handle_sense()
{
	string reply;
	
	if (strstr(get_message(), "Sense_Idle_Workers") != NULL)
	{
		bool idle_workers = Sense_Idle_Workers();

		if (idle_workers)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Mineral_Count") != NULL)
	{
		reply = to_string(Sense_Mineral_Count());
	}
	else if (strstr(get_message(), "Sense_Gas_Count") != NULL)
	{
		reply = to_string(Sense_Gas_Count());
	}
	else if (strstr(get_message(), "Sense_Geyser_Count") != NULL)
	{
		reply = to_string(Sense_Geyser_Count());
	}
	else if (strstr(get_message(), "Sense_Worker_Count") != NULL)
	{
		reply = to_string(Sense_Worker_Count());
	}
	else if (strstr(get_message(), "Sense_Marine_Count") != NULL)
	{
		reply = to_string(Sense_Marine_Count());
	}
	else if (strstr(get_message(), "Sense_Medic_Count") != NULL)
	{
		reply = to_string(Sense_Medic_Count());
	}
	else if (strstr(get_message(), "Sense_Barracks_Count") != NULL)
	{
		reply = to_string(Sense_Barracks_Count());
	}
	else if (strstr(get_message(), "Sense_Need_Supply") != NULL)
	{
		bool result = Sense_Need_Supply();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Building_In_Progress") != NULL)
	{
		bool result = Sense_Building_In_Progress();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Building_Supply") != NULL)
	{
		bool result = Sense_Building_Supply();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Building_Barracks") != NULL)
	{
		bool result = Sense_Building_Barracks();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Have_Barracks") != NULL)
	{
		bool result = Sense_Have_Barracks();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Building_Academy") != NULL)
	{
		bool result = Sense_Building_Academy();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Have_Academy") != NULL)
	{
		bool result = Sense_Have_Academy();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Can_Produce_Supply") != NULL)
	{
		bool result = Sense_Can_Produce_Supply();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Can_Produce_Refinery") != NULL)
	{
		bool result = Sense_Can_Produce_Refinery();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Can_Produce_Barracks") != NULL)
	{
		bool result = Sense_Can_Produce_Barracks();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Can_Produce_Academy") != NULL)
	{
		bool result = Sense_Can_Produce_Academy();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Can_Produce_SCV") != NULL)
	{
		bool result = Sense_Can_Produce_SCV();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Can_Produce_Marine") != NULL)
	{
		bool result = Sense_Can_Produce_Marine();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}
	else if (strstr(get_message(), "Sense_Can_Produce_Medic") != NULL)
	{
		bool result = Sense_Can_Produce_Medic();

		if (result)
		{
			reply = "True";
		}
		else
		{
			reply = "False";
		}
	}

	return reply;
}

void BWAPIBot::Action_Manage_Workers()
{
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	Unitset minerals = BWAPI::Broodwar->getMinerals();
	Unitset workers;
	Unitset refineries;
	int refinery_worker_count = 0;

	// Get set of workers and refineries
	for (auto &u : units)
	{
		if (u->getType().isRefinery() && u->isCompleted())
		{
			refineries.insert(u);
		}
		else if (u->getType().isWorker())
		{
			workers.insert(u);
		}
	}

	// Ensure each refinery has 3 workers on it
	for (auto &r : refineries)
	{
		refinery_worker_count = 0;

		// Calculate number of workers gathering gas from this refinery
		for (auto &w : workers)
		{
			if (w->isGatheringGas() && w->getTarget()->getID() == r->getID())
			{
				refinery_worker_count++;
			}
		}

		// If there are less than than 3 workers gathering gas from here
		if (refinery_worker_count < 3)
		{
			// Transfer required number of workers from mineral gathering
			for (auto &w : workers)
			{
				if (w->isGatheringMinerals())
				{
					w->gather(r);
					refinery_worker_count--;
				}

				if (refinery_worker_count == 0)
				{
					break;
				}
			}
		}
	}

	// Send any idle workers to mine
	for (auto &w : workers)
	{
		if (w->isIdle())
		{
			Unit closestMineral = nullptr;

			for (auto &m : minerals)
			{
				if (!closestMineral || w->getDistance(m) < w->getDistance(closestMineral))
				{
					closestMineral = m;
				}
			}

			if (closestMineral)
			{
				w->gather(closestMineral);
			}
		}
	}
}

void BWAPIBot::Action_Send_Idle_Workers_To_Mine()
{
	//send each worker to the mineral field that is closest to it
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	Unitset minerals = BWAPI::Broodwar->getMinerals();

	for (auto &u : units)
	{
		if (u->getType().isWorker() && u->isIdle())
		{
			Unit closestMineral = nullptr;

			for (auto &m : minerals)
			{
				if (!closestMineral || u->getDistance(m) < u->getDistance(closestMineral))
				{
					closestMineral = m;
				}
			}

			if (closestMineral)
			{
				u->gather(closestMineral);
			}
		}
	}
}

void BWAPIBot::Action_Produce_SCV()
{
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->isIdle() && u->getType().isResourceDepot())
		{
			u->train(u->getType().getRace().getWorker());
		}
	}
}

void BWAPIBot::Action_Produce_Marine()
{
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType() == BWAPI::UnitTypes::Terran_Barracks && u->isCompleted() && u->isTraining() == false)
		{
			u->train(BWAPI::UnitTypes::Terran_Marine);
			return;
		}
	}
}

void BWAPIBot::Action_Produce_Medic()
{
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType() == BWAPI::UnitTypes::Terran_Barracks && u->isCompleted() && u->isTraining() == false)
		{
			u->train(BWAPI::UnitTypes::Terran_Medic);
			return;
		}
	}
}

void BWAPIBot::Action_Build_Supply()
{
	BWAPI::TilePosition desired_location = BWAPI::Broodwar->self()->getStartLocation();
	BWAPI::TilePosition build_location = BWAPI::Broodwar->getBuildLocation(BWAPI::UnitTypes::Terran_Supply_Depot,
																		   BWAPI::Broodwar->self()->getStartLocation(),
																		   32);
	
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType().isWorker() && u->isGatheringMinerals())
		{
			u->build(BWAPI::UnitTypes::Terran_Supply_Depot, build_location);
			
			building_begin = BWAPI::Broodwar->elapsedTime();
			building_in_progress = true;

			return;
		}
	}
}

void BWAPIBot::Action_Build_Refinery()
{
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	Unitset geysers = BWAPI::Broodwar->getGeysers();

	for (auto &u : units)
	{
		if (u->getType().isWorker() && u->isGatheringMinerals())
		{
			for (auto &g : geysers)
			{
				u->build(BWAPI::UnitTypes::Terran_Refinery, g->getTilePosition());

				building_begin = BWAPI::Broodwar->elapsedTime();
				building_in_progress = true;

				return;
			}
		}
	}
}

void BWAPIBot::Action_Build_Barracks()
{
	BWAPI::TilePosition desired_location = BWAPI::Broodwar->self()->getStartLocation();
	BWAPI::TilePosition build_location = BWAPI::Broodwar->getBuildLocation(BWAPI::UnitTypes::Terran_Barracks,
																		   BWAPI::Broodwar->self()->getStartLocation(),
																		   32);

	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType().isWorker() && u->isGatheringMinerals())
		{
			u->build(BWAPI::UnitTypes::Terran_Barracks, build_location);

			building_begin = BWAPI::Broodwar->elapsedTime();
			building_in_progress = true;

			return;
		}
	}
}

void BWAPIBot::Action_Build_Academy()
{
	BWAPI::TilePosition desired_location = BWAPI::Broodwar->self()->getStartLocation();
	BWAPI::TilePosition build_location = BWAPI::Broodwar->getBuildLocation(BWAPI::UnitTypes::Terran_Academy,
																		   BWAPI::Broodwar->self()->getStartLocation(),
																		   32);

	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType().isWorker() && u->isGatheringMinerals())
		{
			u->build(BWAPI::UnitTypes::Terran_Academy, build_location);

			building_begin = BWAPI::Broodwar->elapsedTime();
			building_in_progress = true;

			return;
		}
	}
}

int BWAPIBot::Sense_Mineral_Count()
{
	return BWAPI::Broodwar->self()->minerals();
}

int BWAPIBot::Sense_Gas_Count()
{
	return BWAPI::Broodwar->self()->gas();
}

int BWAPIBot::Sense_Geyser_Count()
{
	Unitset geysers = BWAPI::Broodwar->getGeysers();

	return geysers.size();
}

int BWAPIBot::Sense_Worker_Count()
{
	int worker_count = 0;
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType().isWorker())
		{
			worker_count++;;
		}
	}

	return worker_count;
}

int BWAPIBot::Sense_Marine_Count()
{
	int marine_count = 0;
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType() == BWAPI::UnitTypes::Terran_Marine)
		{
			marine_count++;;
		}
	}

	return marine_count;
}

int BWAPIBot::Sense_Medic_Count()
{
	int medic_count = 0;
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType() == BWAPI::UnitTypes::Terran_Medic)
		{
			medic_count++;;
		}
	}

	return medic_count;
}

int BWAPIBot::Sense_Barracks_Count()
{
	int barracks_count = 0;
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType() == BWAPI::UnitTypes::Terran_Barracks)
		{
			barracks_count++;;
		}
	}

	return barracks_count;
}

bool BWAPIBot::Sense_Idle_Workers()
{
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType().isWorker() && u->canGather() && u->isIdle())
		{
			return true;
		}
	}

	return false;
}

bool BWAPIBot::Sense_Building_In_Progress()
{
	return building_in_progress;
}

bool BWAPIBot::Sense_Need_Supply()
{
	// Scale difference between max and used based on time in game
	// to compensate for faster production times
	int supply_used = BWAPI::Broodwar->self()->supplyUsed();
	int supply_max = BWAPI::Broodwar->self()->supplyTotal();

	if (supply_max == 400)
	{
		return false;
	}
	else if (supply_used >= (supply_max - 4))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool BWAPIBot::Sense_Building_Supply()
{
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if ((u->getType().isWorker()) && u->getBuildUnit() != NULL)
		{
			if ((u->getBuildUnit()->getType() == BWAPI::UnitTypes::Terran_Supply_Depot))
			{
				return true;
			}
		}
	}

	return false;
}

bool BWAPIBot::Sense_Building_Barracks()
{
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if ((u->getType().isWorker()) && u->getBuildUnit() != NULL)
		{
			if ((u->getBuildUnit()->getType() == BWAPI::UnitTypes::Terran_Barracks))
			{
				return true;
			}
		}
	}

	return false;
}

bool BWAPIBot::Sense_Have_Barracks()
{
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType() == BWAPI::UnitTypes::Terran_Barracks && u->isCompleted())
		{
			return true;
		}
	}

	return false;
}

bool BWAPIBot::Sense_Building_Academy()
{
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if ((u->getType().isWorker()) && u->getBuildUnit() != NULL)
		{
			if ((u->getBuildUnit()->getType() == BWAPI::UnitTypes::Terran_Academy))
			{
				return true;
			}
		}
	}

	return false;
}

bool BWAPIBot::Sense_Have_Academy()
{
	Unitset units = BWAPI::Broodwar->self()->getUnits();
	for (auto &u : units)
	{
		if (u->getType() == BWAPI::UnitTypes::Terran_Academy && u->isCompleted())
		{
			return true;
		}
	}

	return false;
}

bool BWAPIBot::Sense_Can_Produce_Supply()
{
	return BWAPI::Broodwar->canMake(BWAPI::UnitTypes::Terran_Supply_Depot);
}

bool BWAPIBot::Sense_Can_Produce_Refinery()
{
	BWAPI::Unitset geysers = BWAPI::Broodwar->getGeysers();
	bool canMake = BWAPI::Broodwar->canMake(BWAPI::UnitTypes::Terran_Refinery);

	// Can make a refinery and there are geysers available
	if (geysers.size() > 0 && canMake)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool BWAPIBot::Sense_Can_Produce_Barracks()
{
	return BWAPI::Broodwar->canMake(BWAPI::UnitTypes::Terran_Barracks);
}

bool BWAPIBot::Sense_Can_Produce_Academy()
{
	return BWAPI::Broodwar->canMake(BWAPI::UnitTypes::Terran_Academy);
}

bool BWAPIBot::Sense_Can_Produce_SCV()
{
	return BWAPI::Broodwar->canMake(BWAPI::UnitTypes::Terran_SCV);
}

bool BWAPIBot::Sense_Can_Produce_Marine()
{
	return BWAPI::Broodwar->canMake(BWAPI::UnitTypes::Terran_Marine);
}

bool BWAPIBot::Sense_Can_Produce_Medic()
{
	return BWAPI::Broodwar->canMake(BWAPI::UnitTypes::Terran_Medic);
}