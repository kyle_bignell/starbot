#include <BWAPI.h>
#include <BWAPI/Client.h>

#include <iostream>
#include <string>

#include "BWAPIBot.h"

using namespace std;
using namespace BWAPI;

int main(int argc, const char* argv[])
{
	BWAPIBot bot;
	string reply;

	while(true)
	{
		bot.enter_match();
		
		// Tell POSH client we are ready to start
		bot.send_message("Init");

		while(BWAPI::Broodwar->isInGame())
		{
			// Read command
			if (bot.read_message() != true)
			{
				break;
			}

			// Handle messages
			reply = bot.handle_message();
			
			// Send reply
			bot.send_message(reply);

			bot.reset_message();
			
			// Handle in game events
			bot.handle_game_events();

			// Updates internal timers
			bot.update();

			// Update game (should be in seperate thread)
			BWAPI::BWAPIClient.update();

			if (!BWAPI::BWAPIClient.isConnected())
			{
				bot.reconnect();
			}
		}

		std::cout << "Game ended" << std::endl;
	}

	std::cout << "Press ENTER to continue..." << std::endl;
	std::cin.ignore();
	return 0;
}