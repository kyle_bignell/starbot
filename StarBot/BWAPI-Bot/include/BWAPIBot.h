#pragma once
#include <BWAPI.h>
#include <BWAPI/Client.h>
#include <boost/asio.hpp>
#include <boost/array.hpp>

#include <iostream>
#include <thread>
#include <chrono>
#include <string>

using namespace std;
using namespace BWAPI;
using boost::asio::ip::tcp;

class BWAPIBot
{
	public:
		BWAPIBot();
		~BWAPIBot();
		void reconnect();
		void enter_match();
		bool send_message(string);
		bool read_message();
		char* get_message();
		string handle_message();
		string handle_action();
		string handle_sense();
		void reset_message();
		void handle_game_events();
		void update();

		void drawStats();
		void drawBullets();
		void drawVisibilityData();
		void showPlayers();
		void showForces();

		// Actions
		void Action_Manage_Workers();
		void Action_Send_Idle_Workers_To_Mine();
		void Action_Produce_SCV();
		void Action_Produce_Marine();
		void Action_Produce_Medic();
		void Action_Build_Supply();
		void Action_Build_Refinery();
		void Action_Build_Barracks();
		void Action_Build_Academy();

		// Senses
		int Sense_Mineral_Count();
		int Sense_Gas_Count();
		int Sense_Geyser_Count();
		int Sense_Worker_Count();
		int Sense_Marine_Count();
		int Sense_Medic_Count();
		int Sense_Barracks_Count();
		bool Sense_Idle_Workers();
		bool Sense_Building_In_Progress();
		bool Sense_Need_Supply();
		bool Sense_Building_Supply();
		bool Sense_Building_Barracks();
		bool Sense_Have_Barracks();
		bool Sense_Building_Academy();
		bool Sense_Have_Academy();
		bool Sense_Can_Produce_Supply();
		bool Sense_Can_Produce_Refinery();
		bool Sense_Can_Produce_Barracks();
		bool Sense_Can_Produce_Academy();
		bool Sense_Can_Produce_SCV();
		bool Sense_Can_Produce_Marine();
		bool Sense_Can_Produce_Medic();

	private:
		boost::asio::io_service io_service;
		tcp::socket socket = tcp::socket(io_service);
		boost::array<char, 128> message;
		boost::system::error_code error;

		bool show_bullets;
		bool show_visibility_data;
		// Time in seconds that building order was issued
		int building_begin;
		// How long to wait until the building in progress flag is reset
		int building_wait_time;
		bool building_in_progress;
};

